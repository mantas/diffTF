![logo|small](/docs/logo.png "diffTF logo")


Genome-wide quantification of differential transcription factor activity: diffTF         
=====================================================================================================

Thank you for the interest in diffTF! If you have questions or comments, feel free to contact us. We will be happy to answer any questions related to this project as well as questions related to the software implementation.

Summary
-------------
Transcription factor (TF) activity constitutes an important readout of cellular signalling pathways and thus for assessing regulatory differences across conditions. However, current technologies lack the ability to simultaneously assessing activity changes for multiple TFs and surprisingly little is known about whether a TF acts as repressor or activator. To this end, we introduce the widely applicable genome-wide method diffTF to assess differential TF binding activity and classifying TFs as activator or repressor by integrating any type of genome-wide chromatin with RNA-Seq data and in-silico predicted TF binding sites

Documentation
-------------

[A detailed Documentation is available here](http://diffTF.readthedocs.io)

Installation and Quick Start
-------------------------------

[Please see the Documentation for easy "Installation and Quick Start" instructions.](http://difftf.readthedocs.io/en/latest/chapter1.html)


Citation
--------
*Please cite the following article if you use diffTF in your research*:

Ivan Berest*, Christian Arnold*, Armando Reyes-Palomares, Kasper Rassmussen & Judith B. Zaugg. Genome-wide quantification of differential transcription factor activity: diffTF. 2017. submitted.
